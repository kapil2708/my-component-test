import { GbaseCorePage } from './app.po';

describe('gbase-core App', function() {
  let page: GbaseCorePage;

  beforeEach(() => {
    page = new GbaseCorePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
